from django.core.urlresolvers import reverse
from django.test import TestCase
from django.utils import timezone
import tempfile

from .models import Category, Post, PostImage

class CategoryModelTest(TestCase):
	def test_category_creation(self):
		category = Category.objects.create(
			name="Test Category",
			slug="test-category",
		)
		self.assertEqual(category.slug, 'test-category')

class PostModelTest(TestCase):
	def test_post_creation(self):
		post = Post.objects.create(
			title="Test Post",
			slug="test-post",
			body="Test post content.",
		)
		now = timezone.now()
		self.assertLess(post.created_at, now)

class PostImageModelTest(TestCase):
	def setUp(self):
		self.post = Post.objects.create(
			title="Test Post",
			slug="test-post",
			body="Test post content.",
		)

	def test_post_image_creation(self):
		post_image = PostImage.objects.create(
			name="Test Image",
			post=self.post,
			image=tempfile.NamedTemporaryFile(suffix='.jpg').name
		)
		now = timezone.now()
		self.assertLess(post_image.created_at, now)
		self.assertEqual(post_image.post, self.post)

class PostViewsTests(TestCase):
	def setUp(self):
		self.post = Post.objects.create(
			title="Test Post",
			slug="test-post",
			body="Test post content.",
		)
		self.post2 = Post.objects.create(
			title="Test Post2",
			slug="test-post-2",
			body="Test post 2 content.",
		)

	def test_portfolio_index_view(self):
		resp = self.client.get(reverse('blog:index'))
		self.assertEqual(resp.status_code, 200)
		self.assertIn(self.post, resp.context['posts'])
		self.assertIn(self.post2, resp.context['posts'])

	def test_portfolio_detail_view(self):
		resp = self.client.get(reverse('blog:detail',
										kwargs={'pk': self.post.pk}))
		self.assertEqual(resp.status_code, 200)
		self.assertEqual(self.post, resp.context['post'])

