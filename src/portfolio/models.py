import datetime

from django.core.files.base import ContentFile
from django.db import models
from django.db.models import Q
from io import BytesIO
from markdownx.models import MarkdownxField
from resizeimage import resizeimage
from PIL import Image


class ProjectType(models.Model):
	"""
	Project types (Design, Development, Web, and others)
	"""
	name = models.CharField(max_length=255,
							unique=True)
	slug = models.SlugField(unique=True)

	class Meta:
		ordering = ['name',]
	
	def __str__(self):
		return self.name

	def __unicode__(self):
		return self.name

class ProjectQuerySet(models.QuerySet):
	def published(self):
		return self.filter(is_published=True)
		

class Project(models.Model):
	"""
	A project with a title, description, url slug, links, 
	associated types, and media references

	"""
	title = models.CharField(max_length=255,
							 unique=True)
	slug = models.SlugField(unique=True)
	tagline = models.CharField(max_length=255, blank=True, 
								default='')
	description = MarkdownxField(blank=True, default='')
	live_url = models.URLField(max_length=2000, blank=True, 
								default='')
	git_url = models.URLField(max_length=2000, blank=True, 
								default='')
	types = models.ManyToManyField(ProjectType)
	created_at = models.DateTimeField(auto_now_add=True, 
									  db_index=True, editable=False)
	modified_at = models.DateTimeField(auto_now=True, editable=False)
	is_published = models.BooleanField(default=False)
	published_at = models.DateTimeField(blank=True, null=True, 
										db_index=True, editable=False)

	objects = ProjectQuerySet.as_manager()

	class Meta:
		verbose_name = "Portfolio Project"
		verbose_name_plural = "Portfolio Projects"
		ordering = ['-created_at',]

	def __str__(self):
		return self.title

	def __unicode__(self):
		return self.title

	def save(self, **kwargs):
		if self.is_published and self.published_at is None:
			self.published_at = datetime.datetime.now()
		else:
			self.published_at = None
		super(Project, self).save(**kwargs)

class ProjectImage(models.Model):
	"""
	Images and screenshots for portfolio projects
	"""
	name = models.CharField(max_length=255)
	project = models.ForeignKey(Project, related_name='images')
	image = models.ImageField(upload_to='projects/images/',
							  help_text="Minimum size: 1496x1024.")
	thumb = models.ImageField(upload_to='projects/thumbs/', 
							  editable=False, blank=True, null=True)
	created_at = models.DateTimeField(auto_now_add=True)

	class Meta:
		verbose_name = "Project Image"
		verbose_name_plural = "Project Images"
		ordering = ['project',]

	def __str__(self):
		return self.name

	def __unicode__(self):
		return self.name

	def save(self, *args, **kwargs):
		pil_image_obj = Image.open(self.image)

		width, height = pil_image_obj.size

		new_image = resizeimage.resize_cover(pil_image_obj, [1496, 1024])
		new_thumb = resizeimage.resize_cover(pil_image_obj, [265, 182])

		new_image_io = BytesIO()
		new_image.save(new_image_io, pil_image_obj.format)

		temp_name = self.image.name
		self.image.delete(save=False)

		self.image.save(
			temp_name,
			content=ContentFile(new_image_io.getvalue()),
			save=False,
		)

		new_thumb_io = BytesIO()
		new_thumb.save(new_thumb_io, pil_image_obj.format)

		self.thumb.save(
			temp_name,
			content=ContentFile(new_thumb_io.getvalue()),
			save=False,
		)

		super(ProjectImage, self).save(*args, **kwargs)