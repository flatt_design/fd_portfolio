import markdown
from django import template 
from django.conf import settings

register = template.Library()

@register.filter(name='markdownify')
def markdownify(content):
	return markdown.markdown(content, extensions=settings.MARKDOWNX_MARKDOWN_EXTENSIONS,
							extension_configs=settings.MARKDOWNX_MARKDOWN_EXTENSION_CONFIGS)

@register.filter(name='inlist')
def inlist(value, list_):
	return value in list_.split(',')