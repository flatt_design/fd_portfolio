from django.shortcuts import get_object_or_404, render

from .models import Project, ProjectType

def index(request):
	"""
	All available portfolio projects
	"""
	projects = Project.objects.published()
	project_types = ProjectType.objects.all()

	context = {
		'projects': projects,
		'project_types': project_types
	}

	return render(request, 'portfolio/index.html', context)

def detail(request, pk):
	"""
	View a single portfolio project
	"""
	project = get_object_or_404(Project, pk=pk)

	context = {
		'project': project
	}

	return render(request, 'portfolio/detail.html', context)