from django.shortcuts import get_object_or_404, render

from .models import Post, Category, POST_TYPES

def index(request):
	"""
	All blog posts
	"""
	posts = Post.objects.published()
	tags = Category.objects.all()
	types = POST_TYPES

	context = {
		'posts': posts,
		'tags': tags,
		'types': types,
	}

	return render(request, 'blog/index.html', context)

def category(request, tag):
	"""
	Blog posts by category
	"""
	posts = Post.objects.published()
	tags = Category.objects.exclude(slug=tag)

	context = {
		'posts': posts,
		'tags': tags
	}

	return render(request, 'blog/index.html', context)

def detail(request, pk):
	"""
	Single post detail
	"""
	post = get_object_or_404(Post, pk=pk)

	context = {
		'post': post,
	}
	return render(request, 'blog/detail.html', context)