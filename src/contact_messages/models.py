from django.db import models


from django.db import models

class ContactMessage(models.Model):
	"""
	Responses to our contact form

	"""

	name = models.CharField(max_length=255,)
	email = models.EmailField()
	website = models.URLField(blank=True, default='',)
	subject = models.CharField(max_length=255,)
	message = models.TextField()
	is_read = models.BooleanField(default=False)
	created_at = models.DateTimeField(auto_now_add=True)

	class Meta:
		verbose_name = "Message"
		verbose_name_plural = "Messages"
		ordering = ['is_read',]

	def __str__(self):
		return self.email

	def save(self, **kwargs):
		super(ContactMessage, self).save(**kwargs)

