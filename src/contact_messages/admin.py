from django.contrib import admin

from .models import ContactMessage

class ContactMessageAdmin(admin.ModelAdmin):
	list_display = ["__str__", "subject", "read"]
	readonly_fields = ["name", "email", "website", "subject", "message"]

	def read(self, obj):
		if obj.is_read:
			return True
		else:
			return False

admin.site.register(ContactMessage, ContactMessageAdmin)
