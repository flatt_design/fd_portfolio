from blog.models import Post
from portfolio.models import Project  

def base_template_vars(request):
	
	recent_four = Post.objects.published().recent_four()
	featured_post = Post.objects.published().featured()
	latest_links = Post.objects.published().latest_links()
	nav_latest_projects = Project.objects.published()[:4]
	feature_projects = Project.objects.published()[:2]
	other_projects = Project.objects.published()[2:]
	
	return {
		'recent_four': recent_four,
		'featured_post': featured_post,
		'latest_links': latest_links,
		'nav_latest_projects': nav_latest_projects,
		'feature_projects': feature_projects,
		'other_projects': other_projects,
	}