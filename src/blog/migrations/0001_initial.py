# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import markdownx.models


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Author',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('display_name', models.CharField(unique=True, max_length=255)),
                ('slug', models.SlugField(unique=True)),
                ('bio', models.CharField(help_text=b'Display begins with display name.', unique=True, max_length=255)),
                ('title', models.CharField(default=b'', max_length=255)),
                ('is_staff', models.BooleanField(default=False)),
                ('staff_quote', models.CharField(default=b'', max_length=255, null=True, blank=True)),
                ('website_url', models.URLField(default=b'', max_length=2000, null=True, blank=True)),
                ('facebook_url', models.URLField(default=b'', max_length=2000, null=True, blank=True)),
                ('twitter_url', models.URLField(default=b'', max_length=2000, null=True, blank=True)),
                ('linkedin_url', models.URLField(default=b'', max_length=2000, null=True, blank=True)),
                ('image', models.ImageField(help_text=b'Minimum Size: 200x200', upload_to=b'blog/authors')),
            ],
            options={
                'verbose_name': 'Blog Author',
                'verbose_name_plural': 'Blog Authors',
            },
        ),
        migrations.CreateModel(
            name='Category',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(unique=True, max_length=255)),
                ('slug', models.SlugField(unique=True)),
            ],
            options={
                'ordering': ['name'],
                'verbose_name': 'Blog Category',
                'verbose_name_plural': 'Blog Categories',
            },
        ),
        migrations.CreateModel(
            name='Post',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(unique=True, max_length=255)),
                ('slug', models.SlugField(unique=True)),
                ('tagline', models.CharField(default=b'', max_length=255, blank=True)),
                ('body', markdownx.models.MarkdownxField(default=b'', blank=True)),
                ('post_type', models.CharField(max_length=3, choices=[(b'STD', b'Standard'), (b'IMG', b'Image'), (b'GAL', b'Gallery'), (b'QUO', b'Quote'), (b'LNK', b'Link')])),
                ('quote_text', models.TextField(null=True, blank=True)),
                ('quote_name', models.CharField(max_length=255, null=True, blank=True)),
                ('link_text', models.CharField(max_length=255, null=True, blank=True)),
                ('link_url', models.URLField(default=b'', max_length=2000, null=True, blank=True)),
                ('created_at', models.DateTimeField(auto_now_add=True, db_index=True)),
                ('modified_at', models.DateTimeField(auto_now=True)),
                ('is_published', models.BooleanField(default=False)),
                ('published_at', models.DateTimeField(db_index=True, null=True, editable=False, blank=True)),
                ('author', models.ForeignKey(blank=True, to='blog.Author', null=True)),
                ('categories', models.ManyToManyField(to='blog.Category')),
            ],
            options={
                'ordering': ['-created_at'],
                'verbose_name': 'Blog Post',
                'verbose_name_plural': 'Blog Posts',
            },
        ),
        migrations.CreateModel(
            name='PostImage',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=255)),
                ('image', models.ImageField(help_text=b'Minimum size: 1496x1024.', upload_to=b'blog/images/')),
                ('thumb', models.ImageField(upload_to=b'blog/thumbs/', null=True, editable=False, blank=True)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('post', models.ForeignKey(related_name='images', to='blog.Post')),
            ],
            options={
                'ordering': ['post'],
                'verbose_name': 'Post Image',
                'verbose_name_plural': 'Post Images',
            },
        ),
    ]
