import datetime, logging

from PIL import Image 
from io import BytesIO
from django.core.files.base import ContentFile
from django.db import models
from django.db.models import Q
from markdownx.models import MarkdownxField
from resizeimage import resizeimage

POST_TYPES = (
	('STD', 'Standard'),
	('IMG', 'Image'),
	('GAL', 'Gallery'),
	# ('MED', 'Media'),
	('QUO', 'Quote'),
	('LNK', 'Link'),
)


class Category(models.Model):
	"""
	Tags for blog post categories
	"""
	name = models.CharField(max_length=255,
							unique=True)
	slug = models.SlugField(unique=True)

	class Meta:
		ordering = ['name',]
		verbose_name_plural = "Blog Categories"
		verbose_name = "Blog Category"
	
	def __str__(self):
		return self.name

class PostQuerySet(models.QuerySet):
	def published(self):
		return self.filter(is_published=True)

	def recent_four(self):
		types_qs = Q()
		types_list = ['STD', 'IMG', 'GAL']

		for post_type in types_list:
			types_qs = types_qs | Q(post_type=post_type)

		return self.filter(types_qs)[:4]

	def featured(self):
		if self.filter(post_type='GAL').exists():
			return self.filter(post_type='GAL').first()
		elif self.filter(post_type='IMG').exists():
			return self.filter(post_type='IMG').first()
		else:
			return self.filter(post_type='STD').first()

	def latest_links(self):
		return self.filter(post_type='LNK')[:6]

class AuthorQuerySet(models.QuerySet):
	def staff(self):
		return self.filter(is_staff=True)

class Author(models.Model):
	"""
	Blog post authors (Might not always be authenticated users)
	"""
	display_name = models.CharField(max_length=255,
									unique=True)
	slug = models.SlugField(unique=True)
	bio = models.CharField(max_length=255,
						   unique=True,
						   help_text='Display begins with display name.')
	title = models.CharField(max_length=255, default='')
	is_staff = models.BooleanField(default=False)
	staff_quote = models.CharField(max_length=255, blank=True,
								null=True, default='')
	website_url = models.URLField(max_length=2000, blank=True,
							  null=True, default='')
	facebook_url = models.URLField(max_length=2000, blank=True,
							  null=True, default='')
	twitter_url = models.URLField(max_length=2000, blank=True,
							  null=True, default='')
	linkedin_url = models.URLField(max_length=2000, blank=True,
							  null=True, default='')
	image = models.ImageField(upload_to='blog/authors',
							  help_text='Minimum Size: 200x200')

	objects = AuthorQuerySet.as_manager()

	class Meta:
		verbose_name = "Blog Author"
		verbose_name_plural = "Blog Authors"

	def __str__(self):
		return self.display_name

	def save(self, *args, **kwargs):
		pil_image_obj = Image.open(self.image)

		width, height = pil_image_obj.size

		new_image = resizeimage.resize_cover(pil_image_obj, [150, 150])

		new_image_io = BytesIO()
		new_image.save(new_image_io, pil_image_obj.format)

		temp_name = self.image.name
		self.image.delete(save=False)

		self.image.save(
			temp_name,
			content=ContentFile(new_image_io.getvalue()),
			save=False,
		)

		super(Author, self).save(*args, **kwargs)

class Post(models.Model):
	"""
	Blog posts
	"""
	title = models.CharField(max_length=255,
							 unique=True)
	slug = models.SlugField(unique=True)
	tagline = models.CharField(max_length=255,
								blank=True, default='')
	body = MarkdownxField(blank=True, default='')
	categories = models.ManyToManyField(Category)
	post_type = models.CharField(max_length=3, choices=POST_TYPES)
	author = models.ForeignKey(Author, blank=True, null=True)
	quote_text = models.TextField(null=True, blank=True)
	quote_name = models.CharField(max_length=255, null=True,
								  blank=True)
	link_text = models.CharField(max_length=255, null=True, blank=True)
	link_url = models.URLField(max_length=2000, blank=True, 
							   null=True, default='')
	created_at = models.DateTimeField(auto_now_add=True, 
									  db_index=True, editable=False)
	modified_at = models.DateTimeField(auto_now=True, 
									  editable=False)
	is_published = models.BooleanField(default=False)
	published_at = models.DateTimeField(blank=True, null=True, 
										db_index=True, editable=False)

	objects = PostQuerySet.as_manager()

	class Meta:
		verbose_name = "Blog Post"
		verbose_name_plural = "Blog Posts"
		ordering = ['-created_at',]

	def __str__(self):
		return self.title

	def save(self, **kwargs):
		if self.is_published and self.published_at is None:
			self.published_at = datetime.datetime.now()

		super(Post, self).save(**kwargs)


class PostImage(models.Model):
	"""
	Images and screenshots for portfolio projects
	"""
	name = models.CharField(max_length=255)
	post = models.ForeignKey(Post, related_name='images')
	image = models.ImageField(upload_to='blog/images/',
							  help_text="Minimum size: 1496x1024.")
	thumb = models.ImageField(upload_to='blog/thumbs/', 
							  editable=False, blank=True, null=True)
	created_at = models.DateTimeField(auto_now_add=True)

	class Meta:
		verbose_name = "Post Image"
		verbose_name_plural = "Post Images"
		ordering = ['post',]

	def __str__(self):
		return self.name

	def save(self, *args, **kwargs):
		pil_image_obj = Image.open(self.image)

		width, height = pil_image_obj.size

		new_image = resizeimage.resize_cover(pil_image_obj, [1496, 1024])
		new_thumb = resizeimage.resize_cover(pil_image_obj, [265, 182])

		new_image_io = BytesIO()
		new_image.save(new_image_io, pil_image_obj.format)

		temp_name = self.image.name
		self.image.delete(save=False)

		self.image.save(
			temp_name,
			content=ContentFile(new_image_io.getvalue()),
			save=False,
		)

		new_thumb_io = BytesIO()
		new_thumb.save(new_thumb_io, pil_image_obj.format)

		self.thumb.save(
			temp_name,
			content=ContentFile(new_thumb_io.getvalue()),
			save=False,
		)

		super(PostImage, self).save(*args, **kwargs)
