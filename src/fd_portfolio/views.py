from django.contrib import messages
from django.core.urlresolvers import reverse
from django.http import HttpResponseRedirect
from django.shortcuts import render

from contact_messages.models import ContactMessage
from contact_messages.forms import ContactForm
from blog.models import Author

def index(request):
	context = {}
	return render(request, 'index.html', context)

def contact(request):
	form = ContactForm()
	authors = Author.objects.staff()

	if request.method == 'POST':
		form = ContactForm(request.POST)
		if form.is_valid():
			obj = ContactMessage()
			obj.name = form.cleaned_data['name']
			obj.email = form.cleaned_data['email']
			obj.website = form.cleaned_data['website']
			obj.subject = form.cleaned_data['subject']
			obj.message = form.cleaned_data['message']
			obj.save()
			
			messages.add_message(request, messages.SUCCESS,
								'Thank you!')
			return HttpResponseRedirect(reverse('contact'))
	context = {
		'authors': authors,
		'form': form,
	}
	return render(request, 'contact.html', context)

def services(request):
	return render(request, 'services.html')

def team(request):
	team = Author.objects.staff()

	context = {
		'team': team,
	}
	return render(request, 'team.html', context)
