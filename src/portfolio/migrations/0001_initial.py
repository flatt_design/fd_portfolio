# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import markdownx.models


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Project',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(unique=True, max_length=255)),
                ('slug', models.SlugField(unique=True)),
                ('tagline', models.TextField(default=b'', blank=True)),
                ('description', markdownx.models.MarkdownxField(default=b'', blank=True)),
                ('live_url', models.URLField(default=b'', max_length=2000, blank=True)),
                ('git_url', models.URLField(default=b'', max_length=2000, blank=True)),
                ('created_at', models.DateTimeField(auto_now_add=True, db_index=True)),
                ('modified_at', models.DateTimeField(auto_now=True)),
                ('is_published', models.BooleanField(default=False)),
                ('published_at', models.DateTimeField(db_index=True, null=True, editable=False, blank=True)),
            ],
            options={
                'ordering': ['-created_at'],
                'verbose_name': 'Portfolio Project',
                'verbose_name_plural': 'Portfolio Projects',
            },
        ),
        migrations.CreateModel(
            name='ProjectImage',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=255)),
                ('image', models.ImageField(help_text=b'Minimum size: 1496x1024.', upload_to=b'projects/images/')),
                ('thumb', models.ImageField(upload_to=b'projects/thumbs/', null=True, editable=False, blank=True)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('project', models.ForeignKey(related_name='images', to='portfolio.Project')),
            ],
            options={
                'ordering': ['project'],
                'verbose_name': 'Project Image',
                'verbose_name_plural': 'Project Images',
            },
        ),
        migrations.CreateModel(
            name='ProjectType',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(unique=True, max_length=255)),
                ('slug', models.SlugField(unique=True)),
            ],
            options={
                'ordering': ['name'],
            },
        ),
        migrations.AddField(
            model_name='project',
            name='types',
            field=models.ManyToManyField(to='portfolio.ProjectType'),
        ),
    ]
