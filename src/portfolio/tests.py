from django.core.urlresolvers import reverse
from django.test import TestCase
from django.utils import timezone
import tempfile

from .models import Project, ProjectType, ProjectImage

class ProjectModelTest(TestCase):
	def test_project_creation(self):
		project = Project.objects.create(
			title="Test Project for Testing",
			slug="test-project-testing",
			description="This is a test project for testing."
		)
		now = timezone.now()
		self.assertLess(project.created_at, now)

class ProjectTypeModelTest(TestCase):
	def test_project_type_creation(self):
		type = ProjectType.objects.create(
			name="Type Test",
			slug="type-test"
		)
		self.assertEqual(type.slug, "type-test")

class ProjectImageModelTest(TestCase):
	def setUp(self):
		self.project = Project.objects.create(
			title="Test Project for Testing",
			slug="test-project-testing",
			description="This is a test project for testing."
		)
	def test_project_image_creation(self):
		project_image = ProjectImage.objects.create(
			name="Test Image",
			project=self.project,
			image=tempfile.NamedTemporaryFile(suffix=".jpg").name
		)
		now = timezone.now()
		self.assertLess(project_image.created_at, now)
		self.assertEqual(project_image.project, self.project)

class ProjectViewsTests(TestCase):
	def setUp(self):
		self.project = Project.objects.create(
			title="Test Project for Testing",
			slug="test-project-testing",
			description="This is a test project for testing."
		)
		self.project2 = Project.objects.create(
			title="Test Project2 for Testing",
			slug="test-project2-testing",
			description="This is a test project2 for testing."
		)

	def test_full_portfolio_view(self):
		resp = self.client.get(reverse('portfolio:index'))
		self.assertEqual(resp.status_code, 200)
		self.assertIn(self.project, resp.context['projects'])
		self.assertIn(self.project2, resp.context['projects'])

	def test_project_detail_view(self):
		resp = self.client.get(reverse('portfolio:detail',
										kwargs={'pk': self.project.pk}))
		self.assertEqual(resp.status_code, 200)
		self.assertEqual(self.project, resp.context['project'])